# cl_showpos

A [Dalamud](https://github.com/goatcorp/Dalamud) plugin that adds customizable, Source Engine-styled position info. Based off of the cl_showpos command from the Source Engine.
